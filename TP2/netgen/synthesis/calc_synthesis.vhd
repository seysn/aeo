--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: M.81d
--  \   \         Application: netgen
--  /   /         Filename: calc_synthesis.vhd
-- /___/   /\     Timestamp: Wed Sep 20 11:08:05 2017
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm calc -w -dir netgen/synthesis -ofmt vhdl -sim calc.ngc calc_synthesis.vhd 
-- Device	: xc6slx16-3-csg324
-- Input file	: calc.ngc
-- Output file	: /home/m1/seysn/Documents/AEO/TP1/TP2/netgen/synthesis/calc_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: calc
-- Xilinx	: /opt/Xilinx/12.4/ISE_DS/ISE/
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity calc is
  port (
    switches : in STD_LOGIC_VECTOR ( 7 downto 0 ); 
    led : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    sevenseg : out STD_LOGIC_VECTOR ( 6 downto 0 ); 
    anodes : out STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end calc;

architecture Structure of calc is
  signal switches_3_IBUF_0 : STD_LOGIC; 
  signal switches_2_IBUF_1 : STD_LOGIC; 
  signal switches_1_IBUF_2 : STD_LOGIC; 
  signal switches_0_IBUF_3 : STD_LOGIC; 
  signal sevenseg_6_OBUF_4 : STD_LOGIC; 
  signal sevenseg_5_OBUF_5 : STD_LOGIC; 
  signal sevenseg_4_OBUF_6 : STD_LOGIC; 
  signal sevenseg_3_OBUF_7 : STD_LOGIC; 
  signal sevenseg_2_OBUF_8 : STD_LOGIC; 
  signal sevenseg_1_OBUF_9 : STD_LOGIC; 
  signal sevenseg_0_OBUF_10 : STD_LOGIC; 
begin
  Inst_x7seg_Mram_sevenseg51 : LUT4
    generic map(
      INIT => X"6054"
    )
    port map (
      I0 => switches_3_IBUF_0,
      I1 => switches_1_IBUF_2,
      I2 => switches_0_IBUF_3,
      I3 => switches_2_IBUF_1,
      O => sevenseg_5_OBUF_5
    );
  Inst_x7seg_Mram_sevenseg61 : LUT4
    generic map(
      INIT => X"0941"
    )
    port map (
      I0 => switches_1_IBUF_2,
      I1 => switches_2_IBUF_1,
      I2 => switches_3_IBUF_0,
      I3 => switches_0_IBUF_3,
      O => sevenseg_6_OBUF_4
    );
  Inst_x7seg_Mram_sevenseg21 : LUT4
    generic map(
      INIT => X"8082"
    )
    port map (
      I0 => switches_1_IBUF_2,
      I1 => switches_2_IBUF_1,
      I2 => switches_3_IBUF_0,
      I3 => switches_0_IBUF_3,
      O => sevenseg_2_OBUF_8
    );
  Inst_x7seg_Mram_sevenseg41 : LUT4
    generic map(
      INIT => X"02BA"
    )
    port map (
      I0 => switches_0_IBUF_3,
      I1 => switches_1_IBUF_2,
      I2 => switches_2_IBUF_1,
      I3 => switches_3_IBUF_0,
      O => sevenseg_4_OBUF_6
    );
  Inst_x7seg_Mram_sevenseg31 : LUT4
    generic map(
      INIT => X"9086"
    )
    port map (
      I0 => switches_0_IBUF_3,
      I1 => switches_2_IBUF_1,
      I2 => switches_1_IBUF_2,
      I3 => switches_3_IBUF_0,
      O => sevenseg_3_OBUF_7
    );
  sevenseg_1_1 : LUT4
    generic map(
      INIT => X"E228"
    )
    port map (
      I0 => switches_2_IBUF_1,
      I1 => switches_0_IBUF_3,
      I2 => switches_1_IBUF_2,
      I3 => switches_3_IBUF_0,
      O => sevenseg_1_OBUF_9
    );
  sevenseg_0_1 : LUT4
    generic map(
      INIT => X"6414"
    )
    port map (
      I0 => switches_1_IBUF_2,
      I1 => switches_2_IBUF_1,
      I2 => switches_0_IBUF_3,
      I3 => switches_3_IBUF_0,
      O => sevenseg_0_OBUF_10
    );
  switches_3_IBUF : IBUF
    port map (
      I => switches(3),
      O => switches_3_IBUF_0
    );
  switches_2_IBUF : IBUF
    port map (
      I => switches(2),
      O => switches_2_IBUF_1
    );
  switches_1_IBUF : IBUF
    port map (
      I => switches(1),
      O => switches_1_IBUF_2
    );
  switches_0_IBUF : IBUF
    port map (
      I => switches(0),
      O => switches_0_IBUF_3
    );
  sevenseg_6_OBUF : OBUF
    port map (
      I => sevenseg_6_OBUF_4,
      O => sevenseg(6)
    );
  sevenseg_5_OBUF : OBUF
    port map (
      I => sevenseg_5_OBUF_5,
      O => sevenseg(5)
    );
  sevenseg_4_OBUF : OBUF
    port map (
      I => sevenseg_4_OBUF_6,
      O => sevenseg(4)
    );
  sevenseg_3_OBUF : OBUF
    port map (
      I => sevenseg_3_OBUF_7,
      O => sevenseg(3)
    );
  sevenseg_2_OBUF : OBUF
    port map (
      I => sevenseg_2_OBUF_8,
      O => sevenseg(2)
    );
  sevenseg_1_OBUF : OBUF
    port map (
      I => sevenseg_1_OBUF_9,
      O => sevenseg(1)
    );
  sevenseg_0_OBUF : OBUF
    port map (
      I => sevenseg_0_OBUF_10,
      O => sevenseg(0)
    );

end Structure;

