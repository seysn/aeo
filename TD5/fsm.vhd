library IEEE;  
use IEEE.std_logic_1164.all; 
 
entity fsm is  
  port ( clk, reset, x1 : IN std_logic;  
                  outp : OUT std_logic);  
end entity;  
architecture beh1 of fsm is 
  type state_type is (s1,s2,s3,s4);  
  signal state: state_type ;  
begin  
  process (clk,reset)  
  begin  
    if (reset ='1') then  
      state <=s1; outp<='1';  
    elsif (clk='1' and clk'event) then  
      case state is  
        when s1 => if x1='1' then state <= s2;  
                    else          state <= s3;  
                    end if;  
                    outp <= '1';  
        when s2 => state <= s4; outp <= '1';  
        when s3 => state <= s4; outp <= '0';  
        when s4 => state <= s1; outp <= '0'; 
      end case;  
    end if;  
  end process;  
end beh1; 