----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:13:59 09/27/2017 
-- Design Name: 
-- Module Name:    count1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity count1 is
    Port ( sw : in  STD_LOGIC_VECTOR (7 downto 0);
           sum : out  STD_LOGIC_VECTOR (3 downto 0));
end count1;

architecture Behavioral of count1 is
begin
	process (sw)
	variable var : integer;
	begin
		var := 0;
		for I in 0 to 7 loop 
			if sw(I) = '1' then var := var + 1;
			end if;
		end loop;
		sum <= conv_std_logic_vector(var, 4);
	end process;


end Behavioral;

