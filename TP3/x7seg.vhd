----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:33:19 09/20/2017 
-- Design Name: 
-- Module Name:    x7seg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity x7seg is
    Port ( sw : in  STD_LOGIC_VECTOR (4 downto 0);
           sevenseg : out  STD_LOGIC_VECTOR (6 downto 0));
end x7seg;

architecture Behavioral of x7seg is

begin
with sw select
	sevenseg <= "1000000" when "00000",
					"1111001" when "00001",
					"0100100" when "00010",
					"0110000" when "00011",
					"0011001" when "00100",
					"0010010" when "00101",
					"0000010" when "00110",
					"1111000" when "00111",
					"0000000" when "01000",
					"0010000" when "01001",
					"0001000" when "01010",
					"0000011" when "01011",
					"1000110" when "01100",
					"0100001" when "01101",
					"0000110" when "01110",										
					"0001110" when "01111",
					"0001110" when "10000",
					"0000111" when "10001",
					"0001100" when "11000",
					"1001111" when "11001",
					"0000000" when others;
end Behavioral;

