----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:46:18 09/20/2017 
-- Design Name: 
-- Module Name:    calc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity calc is
    Port ( switches : in  STD_LOGIC_VECTOR (7 downto 0);
			  btn : in STD_LOGIC_VECTOR (4 downto 0);
           led : out  STD_LOGIC_VECTOR (7 downto 0);
           sevenseg : out  STD_LOGIC_VECTOR (6 downto 0);
           anodes : out  STD_LOGIC_VECTOR (3 downto 0));
end calc;

architecture Behavioral of calc is

	COMPONENT x7seg
		PORT(
			sw : IN std_logic_vector(4 downto 0);          
			sevenseg : OUT std_logic_vector(6 downto 0)
			);
	END COMPONENT;

	COMPONENT add4
		PORT(
			a : IN std_logic_vector(3 downto 0);
			b : IN std_logic_vector(3 downto 0);          
			sum : OUT std_logic_vector(4 downto 0)
			);
	END COMPONENT;
	
	COMPONENT comp
		PORT(
			sw : IN std_logic_vector(7 downto 0);          
			sum : OUT std_logic
			);
	END COMPONENT;
	
	COMPONENT parity
		PORT(
			sw : IN std_logic_vector(7 downto 0);          
			sum : OUT std_logic
			);
	END COMPONENT;
	
	COMPONENT count1
		PORT(
			sw : IN std_logic_vector(7 downto 0);          
			sum : OUT std_logic_vector(3 downto 0)
			);
	END COMPONENT;

	SIGNAL res : std_logic_vector(4 downto 0);
	SIGNAL addres : std_logic_vector(4 downto 0);
	SIGNAL compres : std_logic;
	SIGNAL parres : std_logic;
	SIGNAL countres : std_logic_vector(3 downto 0);
	
begin
	with btn select
		res <= "0" & switches(7 downto 4) and switches(3 downto 0) when "00001",
				 "0" & switches(7 downto 4) or switches(3 downto 0) when "00010",
				 "0" & switches(7 downto 4) xor switches(3 downto 0) when "00011",
				 "1000" & compres when "00100",
				 "1100" & parres when "01000",
				 "0" & countres when "10000",
				 "0" & addres(3 downto 0) when "00000",
				 "11111" when others;

 	led <= "000" & res;
	anodes <= "1110";
	Inst_x7seg: x7seg PORT MAP(
		sw => res,
		sevenseg => sevenseg
	);

	Inst_add4: add4 PORT MAP(
		a => switches(7 downto 4),
		b => switches(3 downto 0),
		sum => addres
	);
	
	Inst_comp: comp PORT MAP(
		sw => switches(7 downto 0),
		sum => compres
	);
	
	
	Inst_parity: parity PORT MAP(
		sw => switches(7 downto 0),
		sum => parres
	);
	
	Inst_count1: count1 PORT MAP(
		sw => switches(7 downto 0),
		sum => countres
	);

end Behavioral;

